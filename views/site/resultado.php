<?php

use yii\widgets\GridView;

?>

<h2><?=$titulo?></h2>
    
    <p class="lead"><?=$enunciado?></p>
    <div class="well">
        <?= $sql ?>
    </div>

<?= GridView::widget([
    'dataProvider' => $resultados,
    'columns' => $campos
]); ?>

